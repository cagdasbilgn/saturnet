//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace cagdasbilgin.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Uye
    {
        public int Id { get; set; }
        public string UyeAd { get; set; }
        public string UyeSoyad { get; set; }
        public Nullable<long> UyeTCKN { get; set; }
        public string UyeSehir { get; set; }
        public string email { get; set; }
    }
}
