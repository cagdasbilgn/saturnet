﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(cagdasbilgin.Startup))]
namespace cagdasbilgin
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
