﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using cagdasbilgin.Models;
using System.Globalization;

namespace cagdasbilgin.Controllers
{
    public class HomeController : Controller
    {
        saturnetEntities db = new saturnetEntities();
        
        public ActionResult Index()
        {
           
            
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        //public ActionResult Odeme()
        //{
        //    ViewBag.Message = "odeme.";

        //    return View();
        //}
        public ActionResult Abonelik(int id)
        {
            ViewBag.Paket = db.Paket.Where(x => x.Id == id).FirstOrDefault();
            return View();
        }
        [HttpPost]
        public ActionResult Abonelik(string PaketId,string PaketFiyat, string PaketAd , string email,string PaketTur)
        {
          
            if (ModelState.IsValid)
            {

                int pid = Convert.ToInt32(PaketId);
                ViewBag.Paket = db.Paket.Where(x => x.Id == pid).FirstOrDefault();
                int fiyat = Convert.ToInt32(ViewBag.Paket.PaketUcret);
                Siparis siparis = new Siparis();
                siparis.Email = email;
                int tur = Convert.ToInt32(PaketTur);

                siparis.PaketFiyat =fiyat ;
                siparis.PaketTuru = ViewBag.Paket.PaketAd;
                siparis.PaketId = pid;
                db.Siparis.Add(siparis);
                db.SaveChanges();
                TempData["mesaj"] = "Abonelik talebiniz alınmıştır";
            }
        
            return View();
        }
        public ActionResult Paketler()
        {
            var paketler = db.Paket.Where(m=>m.PaketTur==1).ToList();
            //var paketler = db.Paket.ToList(); // gidecek
            return View(paketler);
        }

        public ActionResult Paketler2()
        {
            var paketler = db.Paket.Where(m => m.PaketTur == 2).ToList();
            return View(paketler);
        }
        public ActionResult Slider()
        {
            var model = db.Slider.ToList();
            return View(model);
        }
        public ActionResult Blog()
        {
            var bloglar = db.Blog.ToList();
            return View(bloglar);
        }
        public ActionResult Dil(string dil , string url)
        {
            Session["Culture"] = new CultureInfo(dil);
            return Redirect(url);
        }
    }
}